include(ExternalProject)

if(NOT TARGET catch2_project)
ExternalProject_Add(catch2_project 
    GIT_REPOSITORY https://github.com/catchorg/Catch2.git
    GIT_TAG        v2.11.1
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/catch2
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
)
endif()

ExternalProject_Get_Property(catch2_project SOURCE_DIR)
set(catch2_INCLUDE_DIR ${SOURCE_DIR}/single_include/catch2) 