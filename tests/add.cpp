#include "calculator.h"
#include "catch.hpp"

TEST_CASE("add_int", "[add]")
{
   int        input_number = 2;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.add(5) == 7);
   REQUIRE(my_calculator.add(3) == 10);
   REQUIRE(my_calculator.add(50) == 60);
}

TEST_CASE("add_float", "[add]")
{
   float      input_number = 5.0;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.add(10) == 15.0);
   REQUIRE(my_calculator.add(4) == 19.0);
   REQUIRE(my_calculator.add(11) == 30.0);
}

TEST_CASE("add_double", "[add]")
{
   double     input_number = 10.0;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.add(15) == 25.0);
   REQUIRE(my_calculator.add(5) == 30.0);
   REQUIRE(my_calculator.add(20) == 50.0);
}