#include "calculator.h"
#include "catch.hpp"

TEST_CASE("subtract_int", "[subtract]")
{
   int        input_number = 2;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.subtract(5) == -3);
   REQUIRE(my_calculator.subtract(3) == -6);
   REQUIRE(my_calculator.subtract(50) == -56);
}

TEST_CASE("subtract_float", "[subtract]")
{
   float      input_number = 5.0;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.subtract(10) == -5.0);
   REQUIRE(my_calculator.subtract(4) == -9.0);
   REQUIRE(my_calculator.subtract(11) == -20.0);
}

TEST_CASE("subtract_double", "[subtract]")
{
   double     input_number = 10.0;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.subtract(15) == -5.0);
   REQUIRE(my_calculator.subtract(5) == -10.0);
   REQUIRE(my_calculator.subtract(20) == -30.0);
}