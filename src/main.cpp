#include "calculator.h"
#include <iostream>

int main()
{
   double input_number;
   std::cout << "Enter a number: ";
   std::cin >> input_number;
   calculator my_calculator(input_number);

   uint choice = -1;
   while (choice != 5)
   {
      std::cout << "1. Add" << std::endl;
      std::cout << "2. Subtract" << std::endl;
      std::cout << "3. Multiply" << std::endl;
      std::cout << "4. Divide" << std::endl;
      std::cout << "5. Exit" << std::endl;
      std::cin >> choice;

      switch (choice)
      {
      case 1:
         std::cout << "Enter number: ";
         std::cin >> input_number;
         std::cout << "\nResult: " << my_calculator.add(input_number) << "\n" << std::endl;
         break;
      case 2:
         std::cout << "Enter number: ";
         std::cin >> input_number;
         std::cout << "\nResult: " << my_calculator.subtract(input_number) << "\n" << std::endl;
         break;
      case 3:
         std::cout << "Enter number: ";
         std::cin >> input_number;
         std::cout << "\nResult: " << my_calculator.multiply(input_number) << "\n" << std::endl;
         break;
      case 4:
         std::cout << "Enter number: ";
         std::cin >> input_number;
         std::cout << "\nResult: " << my_calculator.divide(input_number) << "\n" << std::endl;
         break;
      case 5:
         std::cout << "\nExiting." << std::endl;
         break;
      default:
         std::cout << "\nERROR: Incorrect Input\n" << std::endl;
      }
   }
}